﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaPersonas.aspx.cs" Inherits="Practico2.Formulario_web1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h1 style="color:red">Hola Mundo!</h1>

    <asp:TextBox ID="documento" runat="server"></asp:TextBox>
    <asp:Button ID="buscar" runat="server" Text="Buscar" OnClick="buscar_Click" />

<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Practico2NETConnectionString %>" SelectCommand="SELECT * FROM [persona] WHERE ([documento] = @documento)">
    <SelectParameters>
        <asp:ControlParameter ControlID="documento" Name="documento" PropertyName="Text" Type="String" />
    </SelectParameters>
    </asp:SqlDataSource>

<asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" DataKeyNames="id">
    <Columns>
        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
        <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
        <asp:BoundField DataField="apellido" HeaderText="apellido" SortExpression="apellido" />
        <asp:BoundField DataField="documento" HeaderText="documento" SortExpression="documento" />
    </Columns>
    </asp:GridView>



</asp:Content>
